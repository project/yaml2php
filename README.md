## Formatted online version of this README
https://www.drupal.org/project/yaml2php

## Convert a YAML snippet or a YAML file to a PHP formatted array with a simple form

Conversion form at:\
_admin/config/development/yaml2php_

This is a developer utility to generate a code snippet to copy and paste into your own code.

## Features

*   Convert a YAML file or just part of it (pasting a snippet).
*   All fields are optional. It will process the data given, also just the YAML file name.
*   Generated code will pass Drupal CodeSniffer.
*   Follows recommended [best practice](https://www.drupal.org/docs/8/api/configuration-api/simple-configuration-api#s-best-practices "Simple Configuration API | Drupal 8 guide on Drupal.org").
*   Code contains the PHP formatted array separately. Copy and paste what you need.
*   Syntax highlighting of the code.
*   A provided YAML snippet can come from anywhere, even non-Drupal projects.
*   High PHPUnit test coverage (49 assertions).

## Use cases

*   The generated code is the Drupal 8 replacement of what was [variable_set in Drupal 7](https://api.drupal.org/comment/62775#comment-62775 "variable_set | bootstrap.inc | Drupal 7.x | Drupal API").
*   Set a certain configuration if certain conditions are met, wrapping it in an `if` statement.
*   Set a certain configuration dynamically, for example, for all available content types, wrapping it in a `foreach` loop. [Example using a dynamic variable and dependency injection](https://www.drupal.org/docs/8/modules/yaml-to-php/example-using-a-dynamic-variable-and-dependency-injection "Drupal 8 guide on Drupal.org").
*   As part of an automated process to set up an environment with certain configuration through PHP after a Drupal bootstrap. Although in those cases using [Configuration selector](https://www.drupal.org/project/config_selector "Project page on Drupal.org") together with install profiles seems a better fit.

## Usage

Go to the conversion form at _admin/config/development/yaml2php_ (restricted to users with the permission _"Administer site configuration"_). Provide a value for the first field only (YAML file name) or for all three fields (YAML snippet). In case just a YAML file name is provided, it must obviously exist. A snippet can also be taken from another project and will be converted anyway.

The snippet can be only a part of a YAML, either from a file or a copy-paste from the database through the interface at _'admin/config/development/configuration/single/export'_. There is **no need to remove preceding whitespace**. Excessive indenting will be removed automatically. Just copy and paste the needed lines as they are. **Leaving the snippet empty will result in the PHP code for the complete configuration object** taken from the YAML file instead of the database.

As the last step copy and paste the needed part of the PHP snippet to your own code and test locally. The generated working PHP code snippet will write the values to the database when executed. Be aware that it should be followed by a [configuration export and commit](https://modulesunraveled.com/drupal-8-composer-and-configuration-management/exporting-config-locally "Exporting Config Locally | Modules Unraveled").

## Example

See [documentation](https://www.drupal.org/docs/8/modules/yaml-to-php/basic-example "Basic example | Drupal 8 guide on Drupal.org").
