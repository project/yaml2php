<?php

namespace Drupal\yaml2php\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html as HtmlUtility;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Serialization\Yaml;

/**
 * Class ConverterForm.
 */
class ConverterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'converter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $through_interface = Link::fromTextAndUrl($this->t('through the interface'), Url::fromRoute('config.export_single'))->toString();
    // Add CSS.
    $form['#attached']['library'][] = 'yaml2php/yaml2php';
    $form['configuration_object_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configuration object name'),
      '#description' => $this->t('For example &#039;filter.format.full_html&#039;. Accepted with or without a file extension <em>&#039;.yml&#039;</em>.'),
      '#maxlength' => 128,
      '#size' => 64,
    ];
    $form['parent_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Parent keys'),
      '#description' => $this->t('Parent keys from highest to one above the snippet. One per line. Example: &#039;filters&#039;.'),
      '#rows' => 8,
    ];
    $form['yaml_snippet'] = [
      '#type' => 'textarea',
      '#title' => $this->t('YAML snippet'),
      '#description' => $this->t('The snippet can be only a part of a YAML, either from a file or from the database @through_interface. There is no need to remove preceding whitespace.<br /><strong>Leave this field empty to capture the whole configuration object defined in the first field.</strong>', ['@through_interface' => $through_interface]),
      '#rows' => 18,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Convert to PHP'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Validate the object name but only if given.
    if (trim($form_state->getValue('configuration_object_name')) && !preg_match('/^\w[\w\-.]+\w$/', $form_state->getValue('configuration_object_name'))) {
      $form_state->setErrorByName('configuration_object_name', $this->t('Invalid object name.'));
    }

  }

  /**
   * Display converted input at top of form page.
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getValues();
    $type = 'custom';
    $input_config_object = trim(preg_replace('/\.yml$/', '', HtmlUtility::escape($input['configuration_object_name'])));
    // Initialize variable.
    $output = NULL;
    // Rules to sanitize output according to the Drupal coding standard later.
    $target = [
      '/array \(/',
      '/\)/',
      '/true/',
      '/false/',
      '/=>[ \t]+' . PHP_EOL . '[ \t]+\[' . PHP_EOL . '[ \t]+\],/',
      '/=>[ \t]+' . PHP_EOL . '/',
    ];
    $replacement = [
      '[',
      ']',
      'TRUE',
      'FALSE',
      '=> [],',
      '=>' . PHP_EOL,
    ];
    // Generate a page intro with data overview.
    if ($input_config_object) {
      $html = '<h3>' . $this->t('Configuration object name') . '</h3><p>' . $input_config_object . '</p>';
    }
    else {
      $through_interface = Link::fromTextAndUrl($this->t('through the interface'), Url::fromRoute('config.export_single'))->toString();
      $html = '<h3>' . $this->t('Configuration object name') . '</h3><p><span class="yaml2php-warning">' . $this->t('No object name given. Check @through_interface what the correct configuration name is and resubmit, unless you are just using the PHP array.', ['@through_interface' => $through_interface]) . '</span></p>';
    }
    // Process a given YAML snippet.
    if (trim($input['yaml_snippet'])) {
      $parent_keys = array_map('trim', array_values(array_filter(explode(PHP_EOL, HtmlUtility::escape(trim($input['parent_keys']))))));
      $keys = NULL;
      foreach ($parent_keys as $parent_key) {
        $keys .= '<li>' . $parent_key . '</li>' . PHP_EOL;
      }
      if ($parent_keys) {
        $html .= '<h3>' . $this->t('Parent keys') . '</h3><p><ul>' . $keys . '</ul></p>';
      }
      else {
        $html .= '<h3>' . $this->t('Parent keys') . '</h3><p><span class="yaml2php-warning">' . $this->t('No parent keys given. Check if the given YAML snippet has any keys above it under which it is nested.') . '</span></p>';
      }

      // Parse the YAML snippet and convert it to a PHP array.
      $yamlDecoded = Yaml::decode($this->unindent(Xss::filterAdmin($input['yaml_snippet'])));
      if (is_array($yamlDecoded)) {
        // Create the name of the configuration object to set.
        $parent_keys_string = implode('.', $parent_keys);
        // Give an early status message to appear above possible warnings.
        if ($input_config_object && $parent_keys_string) {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-status">Given data converted successfully.</span>'));
        }
        else {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-warning">Given data converted. The PHP array is correct but the last part of the PHP code will probably not work due to empty form fields.</span>'), 'warning');
        }
        // Remove the 'UUID' key. Not needed in the generated code.
        if (isset($yamlDecoded['uuid'])) {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-warning">The \'UUID\' is excluded from conversion. Found value: %value</span>', ['%value' => $yamlDecoded['uuid']]), 'warning');
          unset($yamlDecoded['uuid']);
        }
        // Remove the '_core' key. Not needed in the generated code.
        // See https://www.drupal.org/node/2653358.
        if (isset($yamlDecoded['_core']['default_config_hash'])) {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-warning">The \'default_config_hash\' is excluded from conversion. Found value: %value</span>', ['%value' => $yamlDecoded['_core']['default_config_hash']]), 'warning');
          unset($yamlDecoded['_core']);
        }
        // Generate the PHP code snippet.
        $html .= $this->t('<h2>PHP code to copy and paste</h2>Based on the configuration as set in the database. Remember to export the configuration to the corresponding YAML file and commit.');
        $sets = NULL;
        foreach ($yamlDecoded as $key => $value) {
          $config_object = print_r(var_export($value, TRUE), TRUE);
          $output .= "$$key = " . preg_replace($target, $replacement, $config_object) . ";
";
          $sets .= "  ->set('$parent_keys_string.$key', $$key)
";
        }
        $output .= "\Drupal::configFactory()->getEditable('$input_config_object')
" . $sets . "  ->save();" . PHP_EOL . PHP_EOL;
        $html .= '<pre class="yaml2php-snippet">' . highlight_string("<?php
/**
 * @file
 * Set part of a configuration object in the database.
 *
 * Generated with https://drupal.org/project/yaml2php.
 */
" . $output, TRUE) . '</pre><h2>Submit another below</h2>';
      }
      else {
        $html = $this->t('<span class="yaml2php-error">Unable to convert the given YAML snippet.</span>');
        $type = 'error';
      }
    }
    // Process a given YAML file.
    elseif ($input_config_object) {
      $file = config_get_config_directory(CONFIG_SYNC_DIRECTORY) . '/' . $input_config_object . '.yml';
      if (file_exists($file)) {
        // Parse the YAML file and convert it to a PHP array.
        $yamlDecoded = Yaml::decode(file_get_contents($file));
        // Give an early status message to appear above possible warnings.
        $this->messenger()->addMessage($this->t('<span class="yaml2php-status">The file was found and converted successfully: %file</span>', ['%file' => $file]));
        // Remove the 'UUID' key. Not needed in the generated code.
        if (isset($yamlDecoded['uuid'])) {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-warning">The \'UUID\' is excluded from conversion. Found value: %value</span>', ['%value' => $yamlDecoded['uuid']]), 'warning');
          unset($yamlDecoded['uuid']);
        }
        // Remove the '_core' key. Not needed in the generated code.
        // See https://www.drupal.org/node/2653358.
        if (isset($yamlDecoded['_core']['default_config_hash'])) {
          $this->messenger()->addMessage($this->t('<span class="yaml2php-warning">The \'default_config_hash\' is excluded from conversion. Found value: %value</span>', ['%value' => $yamlDecoded['_core']['default_config_hash']]), 'warning');
          unset($yamlDecoded['_core']);
        }
        // Create a variable name based on the configuration object name.
        $dynamic_var = '$' . str_replace('.', '_', $input_config_object);
        $config_object = print_r(var_export($yamlDecoded, TRUE), TRUE);
        $output .= preg_replace($target, $replacement, $config_object);
        $html .= $this->t('<h2>PHP code to copy and paste</h2>Based on the configuration YAML file: %file', ['%file' => $file]);
        $html .= '<pre class="yaml2php-snippet">' . highlight_string("<?php
/**
 * @file
 * Set a complete configuration object in the database.
 *
 * Generated with https://drupal.org/project/yaml2php.
 */
$dynamic_var = $output;
\Drupal::configFactory()->getEditable('$input_config_object')
  ->setData($dynamic_var)
  ->save();", TRUE) . '</pre><h2>Submit another below</h2>';
      }
      else {
        $html = $this->t('<span class="yaml2php-error">The file does not exist: %file</span>', ['%file' => $file]);
        $type = 'error';
      }
    }
    // Neither a snippet nor a file are given.
    else {
      $html = $this->t('<span class="yaml2php-error">There is nothing to convert.</span>');
      $type = 'error';
    }
    $html = Markup::create($html);
    $this->messenger()->addMessage($html, $type);
  }

  /**
   * Remove excessive indenting.
   *
   * Remove the first space of each line in a loop until any line does not
   * start with a space anymore. Needed to get a valid YAML snippet.
   *
   * @param string $snippet
   *   The snippet to unindent.
   *
   * @return string
   *   A string containing the unindented snippet.
   */
  private function unindent($snippet) {
    // Add a line break to include the first line for space/tab removal.
    $snippet = PHP_EOL . $snippet;
    while (!preg_match('/' . PHP_EOL . '\S/', $snippet)) {
      $snippet = preg_replace('/' . PHP_EOL . '[ \t]/', PHP_EOL, $snippet);
    }
    return trim($snippet);
  }

}
