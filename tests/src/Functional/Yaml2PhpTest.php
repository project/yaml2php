<?php

namespace Drupal\Tests\yaml2php\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test page load and form submission.
 *
 * @group yaml2php
 */
class Yaml2PhpTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['yaml2php'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);
    $this->drupalLogin($this->user);
    $this->copyConfig($this->container->get('config.storage'), $this->container->get('config.storage.sync'));
  }

  /**
   * Test page load.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);

    // Visit the conversion form that requires login with a url object.
    $url = Url::fromRoute('yaml2php.converter_form');
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Test that the form contains the snippet field label.
    $this->assertSession()->pageTextContains('YAML snippet');
  }

  /**
   * Test form submission.
   */
  public function testFormSubmission() {
    // Get form.
    $url = Url::fromRoute('yaml2php.converter_form');
    $this->drupalGet($url);
    // Empty form submission.
    $this->drupalPostForm(NULL, [], 'Convert to PHP');
    $this->assertText('There is nothing to convert.');
    // Form validation testing of the object name.
    $edit = ['configuration_object_name' => 'some space.yml'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Invalid object name.');
    // Form validation testing of the object name.
    $edit = ['configuration_object_name' => '.starting.dot'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Invalid object name.');
    // Form validation testing of the object name.
    $edit = ['configuration_object_name' => 'ending.dot.'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Invalid object name.');
    // Form validation testing of the object name.
    $edit = ['configuration_object_name' => '$tr@nge.ch@r@cter$'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Invalid object name.');
    // Just a configuration object name form submission (does not exist).
    $edit = ['configuration_object_name' => 'non.existing.yml'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('The file does not exist: ');
    // Just a configuration object name form submission (does exist).
    $edit = ['configuration_object_name' => 'core.extension.yml'];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('The file was found and converted successfully: ');
    $this->assertText('The \'default_config_hash\' is excluded from conversion. Found value: ');
    $this->assertRaw('\</span><span style="color: #0000BB">Drupal</span><span style="color: #007700">::</span><span style="color: #0000BB">configFactory</span><span style="color: #007700">()-&gt;</span><span style="color: #0000BB">getEditable</span><span style="color: #007700">(</span><span style="color: #DD0000">\'core.extension\'</span><span style="color: #007700">)');
    // YAML snippet, one field only form submission.
    $edit = [
      'yaml_snippet' => '  filter_htmlcorrector:
    id: filter_htmlcorrector
    provider: filter
    status: true
    weight: 10
    settings: {  }',
    ];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Given data converted. The PHP array is correct but the last part of the PHP code will probably not work due to empty form fields.');
    $this->assertText('No object name given.');
    $this->assertText('No parent keys given.');
    $this->assertRaw('<span style="color: #0000BB">Drupal</span><span style="color: #007700">::</span><span style="color: #0000BB">configFactory</span><span style="color: #007700">()-&gt;</span><span style="color: #0000BB">getEditable</span><span style="color: #007700">(</span><span style="color: #DD0000">\'\'</span><span style="color: #007700">)');
    $this->assertRaw('&nbsp;&nbsp;-&gt;</span><span style="color: #0000BB">set</span><span style="color: #007700">(</span><span style="color: #DD0000">\'.filter_htmlcorrector\'</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$filter_htmlcorrector</span><span style="color: #007700">)');
    // Invalid YAML snippet, one field only form submission.
    $edit = [
      'yaml_snippet' => 'x=y',
    ];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Unable to convert the given YAML snippet.');
    // YAML snippet, all fields form submission. Spaces test function unindent.
    $edit = [
      'configuration_object_name' => 'filter.format.full_html.yml',
      'parent_keys' => 'filters',
      'yaml_snippet' => '  uuid: 3de9c5b1-a90c-43d3-bae7-b2f244b9acae
  filter_caption:
    id: filter_caption
    provider: <a href=\'https://www.drupal.org\'>link</a>
    status: true
    weight: 9
    settings: {  }
  _core:
    default_config_hash: QjC45xw1NlPzm2-BNGnqsDLSN-7O1_PT6xVZ863JRVE
  filter_htmlcorrector:
    id: filter_htmlcorrector
    provider: filter
    status: true
    weight: 10
    settings: {  }',
    ];
    $this->drupalPostForm(NULL, $edit, 'Convert to PHP');
    $this->assertText('Given data converted successfully.');
    $this->assertText('The \'UUID\' is excluded from conversion. Found value: 3de9c5b1-a90c-43d3-bae7-b2f244b9acae');
    $this->assertText('The \'default_config_hash\' is excluded from conversion. Found value: QjC45xw1NlPzm2-BNGnqsDLSN-7O1_PT6xVZ863JRVE');
    $this->assertRaw('<ul><li>filters</li>
</ul>');
    $this->assertRaw("&nbsp;&nbsp;</span><span style=\"color: #DD0000\">'provider'&nbsp;</span><span style=\"color: #007700\">=&gt;&nbsp;</span><span style=\"color: #DD0000\">'&lt;a&nbsp;href=\'https://www.drupal.org\'&gt;link&lt;/a&gt;'</span><span style=\"color: #007700\">,");
    $this->assertRaw('\</span><span style="color: #0000BB">Drupal</span><span style="color: #007700">::</span><span style="color: #0000BB">configFactory</span><span style="color: #007700">()-&gt;</span><span style="color: #0000BB">getEditable</span><span style="color: #007700">(</span><span style="color: #DD0000">\'filter.format.full_html\'</span><span style="color: #007700">)');
    $this->assertRaw('&nbsp;&nbsp;-&gt;</span><span style="color: #0000BB">set</span><span style="color: #007700">(</span><span style="color: #DD0000">\'filters.filter_caption\'</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$filter_caption</span><span style="color: #007700">)');
  }

}
